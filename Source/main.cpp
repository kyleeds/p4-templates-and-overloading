//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include "Array.h"

int main (int argc, const char* argv[])
{
    Array<float> array;
    float newValue;
    while (true)
    {
        std::cin >> newValue;
        
        array.add(newValue);
        
        //Testing if it works
        
        std::cout << "Array Size:" << array.size() << std::endl;
        std::cout << "Last Array Value:" << array.get(array.size()) << std::endl;
        std::cout << "All Array Values:";
        for (int index = 1; index <= array.size(); index += 1)
        {
            std::cout << array.get(index) <<", ";
        }
        std::cout << std::endl;
        
    }
    return 0;
}



