//
//  Array.h
//  CommandLineTool
//
//  Created by Kyle Edwards on 13/10/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

#ifndef Array_h
#define Array_h

#include <stdio.h>

template <class Type>
class Array
{
public:
    /**Constructor*/
    Array()
    {
        numOfArray = 0;
        floatPoint = nullptr;
    };

    /**Detructor*/
    ~Array()
    {
        delete floatPoint;
    };
    
    /**Adds new items to the end of the array*/
    void add (Type itemValue)
    {
        float* tempFloatPoint = new float[numOfArray+1];
        
        for (int index = 0; index < numOfArray; index += 1)
        {
            tempFloatPoint[index] = floatPoint[index];
        }
        
        delete[] floatPoint;
        
        floatPoint = tempFloatPoint;
        floatPoint[numOfArray] = itemValue;
        numOfArray += 1;
        
    };
    
    /**Returns the item at the index*/
    Type get (int index)
    {
        return floatPoint[index-1];
    };
    
    /**returns the number of items currently in the array*/
    Type size ()
    {
        return numOfArray;
    };
    
private:
    int numOfArray;
    float* floatPoint;
};


#endif /* Array_h */
