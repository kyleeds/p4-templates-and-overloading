//
//  Array.hpp
//  CommandLineTool
//
//  Created by Kyle Edwards on 06/10/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

#ifndef LinkedList_hpp
#define LinkedList_hpp

#include <stdio.h>

/** Class for managing dynamic array of floats*/

class Array
{
public:
    /**Constructor*/
    Array();
    
    /**Detructor*/
    ~Array();
    
    /**Adds new items to the end of the array*/
    void add(float intemValue);
    
    /**Returns the item at the index*/
    float get (int index);
    
    /**returns the number of items currently in the array*/
    int size ();
    
private:
    int numOfArray;
    float* floatPoint;
};

#endif /* LinkedList_hpp */
