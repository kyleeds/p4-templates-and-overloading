//
//  LinkedList.cpp
//  CommandLineTool
//
//  Created by Kyle Edwards on 13/10/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

#include "LinkedList.hpp"

LinkedList::LinkedList()
{
    head = nullptr;

}

LinkedList::~LinkedList()
{
    
}

void LinkedList::add (float itemValue)
{
    Node* newNode = new Node;
    newNode->next = nullptr;
    newNode->value = itemValue;
    
    //find end of linked list
    //put last on the end
    
    Node* temp = head;
    
    if (temp == nullptr)
    {
        head = newNode;
    }
    else
    {
        while (temp->next != nullptr)
        {
            temp = temp->next;
        }
        temp->next = newNode;
    }
}

float LinkedList::get (int index)
{
    return 0;
}

int LinkedList::size ()
{
    return 0;
}
