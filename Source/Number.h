//
//  Number.h
//  CommandLineTool
//
//  Created by Kyle Edwards on 13/10/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

#ifndef Number_h
#define Number_h

template <class Type>
class Number
{
public:
    Type get() const
    {
        std::cout << "Value: ";
        return value;
    }
    
    void set(Type newValue)
    {
        value = newValue;
    }
        
private:
    Type value;
};



#endif /* Number_h */
